#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

size_t get_res_len(const char *lhs, const char *rhs, size_t lhs_len, size_t rhs_len, unsigned int base) {

  size_t lhs_len_temp = lhs_len;
  size_t rhs_len_temp = rhs_len;

  if (base == 2) {
    while (lhs_len_temp > 0 && lhs[lhs_len_temp - 1] == '0')
      lhs_len_temp--;
    while (rhs_len_temp > 0 && rhs[rhs_len_temp - 1] == '0')
      rhs_len_temp--; 
  }
  return lhs_len_temp > rhs_len_temp ? lhs_len_temp : rhs_len_temp;
}

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  
  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t res_len =  1 + get_res_len(lhs, rhs, lhs_len, rhs_len, base);

  char *res = (char *)malloc(res_len + 1);
  res[res_len] = '\0';

  int lhs_digit = 0;
  int rhs_digit = 0;
  int carry = 0;
  
  for (size_t i = 0; i < res_len-1; i++) {
    if (lhs_len > 0) {
      lhs_digit = get_digit_value(lhs[lhs_len - 1]);
      lhs_len--;
    } 
    else
      lhs_digit = 0;

    if (rhs_len > 0) {
      rhs_digit = get_digit_value(rhs[rhs_len - 1]);
      rhs_len--;
    }
    else
      rhs_digit = 0;

    int sum = lhs_digit + rhs_digit + carry;
    if (base != 0)
      carry = sum / base;
    else
      carry = 0;
    res[res_len - i - 1] = to_digit(sum % base);
  }

  if (carry > 0)
    res[0] = to_digit(carry);
  else
    memmove(res, res+1, res_len);

  return res;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  if (rhs_len >= lhs_len &&  strcmp(lhs, rhs) < 0)
    return NULL;
  size_t max_len = get_res_len(lhs, rhs, lhs_len, rhs_len, base);

  char *res = (char *)malloc(max_len + 1);
  res[max_len] = '\0';

  int borrow = 0;
  int diff = 0;

  for (size_t i = 0; i < max_len; i++) {
    int lhs_digit = 0;
    int rhs_digit = 0;

    if (i < lhs_len)
      lhs_digit = get_digit_value(lhs[lhs_len - i - 1]);
    else
      lhs_digit = 0;

    if (i < rhs_len)
      rhs_digit = get_digit_value(rhs[rhs_len - i - 1]);
    else
      rhs_digit = 0;

    diff = lhs_digit - rhs_digit - borrow;

    if (diff < 0) {
      diff = (lhs_digit + base) - rhs_digit - borrow;
      borrow = 1;
    }
    else
      borrow = 0;
    res[max_len - i - 1] = to_digit(diff);
  }
  int i = 0;
  for (; res[i] && res[i] == '0' && res[i+1] != '\0'; i++){}
  memmove(res, res+i, max_len);
  return res;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t res_len = lhs_len + rhs_len;

  char *res = (char *)malloc(res_len + 1);
  res[res_len] = '\0';

  for (size_t i = 0; i < res_len; i++) {
    res[i] = '0';
  }

  for (size_t i = 0; i < lhs_len; i++) {
    int carry = 0;
    int lhs_digit = get_digit_value(lhs[lhs_len - i - 1]);

    for (size_t j = 0; j < rhs_len; j++) {
      int rhs_digit = get_digit_value(rhs[rhs_len - j - 1]);
      int product = lhs_digit * rhs_digit + carry + get_digit_value(res[i + j]);

      carry = product / base;
      res[i + j] = to_digit(product % base);
    }

    if (carry > 0) {
      res[i + rhs_len] = to_digit(carry);
    }
  }

  reverse(res);

  int i = 0;
  for (; res[i] && res[i] == '0' && res[i+1] != '\0'; i++){}

  memmove(res, res+i, res_len);
  
  return res;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
